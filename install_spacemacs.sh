#!/bin/bash

#tern is required for emacs/javascript layer
sudo apt-get install npm
#install it globally so emacs can use it
sudo npm install -g tern

#now spacemacs!!!
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d

#link to source-controlled .spacemacs.d where our init.el lives
ln -s ~/spm_dotfiles/.spacemacs.d ~/.spacemacs.d

#eliminate erroneous spacemacs error re. missing directory
mkdir ~/spm_dotfiles/.spacemacs.d/layers



