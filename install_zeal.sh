#!/bin/bash
# DM zeal is the dash doc viewer for linux.
# Useful independently and also callable from emacs dash package.
# Unfortunately UBUNTU main repo version is out-of-date causing persistent
# update nag messages. Get it from zeal-developers instead.
sudo add-apt-repository ppa:zeal-developers/ppa
sudo apt-get update
sudo apt-get install zeal
