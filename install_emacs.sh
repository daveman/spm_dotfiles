#!/bin/bash
# Install emacs 25 on ubuntu 16.04 lts (lts defaults to v 24)
# http://ubuntuhandbook.org/index.php/2017/04/install-emacs-25-ppa-ubuntu-16-04-14-04/
sudo add-apt-repository ppa:kelleyk/emacs
sudo apt update
sudo apt install emacs25



