#!/bin/bash
# Note: install_spacemacs.sh script creates a default ~/.spacemacs file which will take
# precedence over the custom one installed via the script (in ~/.spacemacs.d/init.el)
# until/unless it is deleted or renamed.
install_sourcecode_pro_font.sh
install_emacs.sh
install_spacemacs.sh
