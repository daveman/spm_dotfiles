# DM
# After installing emacs/spacemacs either copy the provided config file to
# ~/.ssh/config or append it to the existing config. This file contains ssh
# settings that permit emacs tramp mode (remote access) to piggyback on a
# persistent connection which is especially helpful when running a remote 
# eshell session.
#
# (Open a separate ssh session to the desired host from another window before
#  connecting from emacs.)

